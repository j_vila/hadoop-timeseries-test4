package com.softtek.blaulabs.reduce;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class TimeSeriesReducerFile extends Reducer<Text, DoubleWritable, Text, DoubleWritable>{
	
	private static final String SUM = "SUM";
	private static final String MAX = "MAX";
	private static final String MIN = "MIN";
	private static final String AVG = "AVG";
	
	@Override
    public void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
		String aggrupation = SUM;
		
		Double sum = 0D;
    	Double max = null;
    	Double min = null;
    	Double count = 0D;
    	
    	for (DoubleWritable val : values) {
    		sum += val.get();
    		max = max == null ? val.get() : (max >= val.get() ? max : val.get()); 
    		min = min == null ? val.get() : (min <= val.get() ? min : val.get());
    		count++;
    	}
    	Double avg = sum / count;
    	
    	Double value = 0D;
    	String newKey = key.toString();
    	if(aggrupation.equals(SUM)){
	    	value = sum;
	    	newKey = newKey + "_" + SUM;
	    }else if(aggrupation.equals(MAX)){
	    	value = max;
	    	newKey = newKey + "_" + MAX;
	    }else if(aggrupation.equals(MIN)){
	    	value = min;
	    	newKey = newKey + "_" + MIN;
	    }else if(aggrupation.equals(AVG)){
	    	value = avg;
	    	newKey = newKey + "_" + AVG;
	    }
    	
    	context.write(new Text(newKey), new DoubleWritable(value));
	}
}
