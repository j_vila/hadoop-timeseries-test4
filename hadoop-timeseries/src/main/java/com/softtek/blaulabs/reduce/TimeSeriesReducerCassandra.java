package com.softtek.blaulabs.reduce;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.cassandra.utils.ByteBufferUtil;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class TimeSeriesReducerCassandra extends Reducer<Text, DoubleWritable, Map<String, ByteBuffer>, List<ByteBuffer>>{

	private static final String COLUMN_SERIE_ID = "key";
	private static final String COLUMN_YEAR = "year";
	private static final String COLUMN_MONTH = "month";
	private static final String COLUMN_TIMESTAMP = "timestamp";
	
	private static final String SUM = "SUM";
	private static final String MAX = "MAX";
	private static final String MIN = "MIN";
	private static final String AVG = "AVG";
	
	@Override
    public void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {		
		Map<String, ByteBuffer> keys = new LinkedHashMap<String, ByteBuffer>();
		//TODO este valor se 
		String aggrupation = SUM;
		
		Double sum = 0D;
    	Double max = null;
    	Double min = null;
    	Double count = 0D;
    	
    	for (DoubleWritable val : values) {
    		sum += val.get();
    		max = max == null ? val.get() : (max >= val.get() ? max : val.get()); 
    		min = min == null ? val.get() : (min <= val.get() ? min : val.get());
    		count++;
    	}
    	Double avg = sum / count;
    	
    	//the key is: serie_id + "_" + timestamp
    	String serieId = key.toString().split("_")[0];
    	Date timestamp = new Date(Long.parseLong(key.toString().split("_")[1]));
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    	cal.setTime(timestamp);
    	
    	keys.put(COLUMN_SERIE_ID, ByteBufferUtil.bytes(serieId));
    	keys.put(COLUMN_YEAR, ByteBufferUtil.bytes(cal.get(Calendar.YEAR)));
    	keys.put(COLUMN_MONTH, ByteBufferUtil.bytes(cal.get(Calendar.MONTH)));
    	keys.put(COLUMN_TIMESTAMP, ByteBufferUtil.bytes(cal.getTimeInMillis()));
    	
    	List<ByteBuffer> variables = new ArrayList<ByteBuffer>();
	    if(aggrupation.equals(SUM)){
	    	variables.add(ByteBufferUtil.bytes(sum));
	    }else if(aggrupation.equals(MAX)){
	    	variables.add(ByteBufferUtil.bytes(max));
	    }else if(aggrupation.equals(MIN)){
	    	variables.add(ByteBufferUtil.bytes(min));
	    }else if(aggrupation.equals(AVG)){
	    	variables.add(ByteBufferUtil.bytes(avg));
	    }
    	
    	context.write(keys, variables);
    }
}
