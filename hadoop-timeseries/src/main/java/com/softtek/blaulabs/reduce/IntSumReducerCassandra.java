package com.softtek.blaulabs.reduce;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.cassandra.utils.ByteBufferUtil;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

//https://git-wip-us.apache.org/repos/asf?p=cassandra.git;a=blob;f=examples/hadoop_cql3_word_count/src/WordCount.java;h=c92f0479bf2db71fc696977048d8bb09496b2dfd;hb=f1004e9b

public class IntSumReducerCassandra extends Reducer<Text, IntWritable, Map<String, ByteBuffer>, List<ByteBuffer>> {
	private Map<String, ByteBuffer> keys;
	private static final String PRIMARY_KEY = "key";
	
	protected void setup(Context context)throws IOException, InterruptedException{
		keys = new LinkedHashMap<String, ByteBuffer>();
	    //String[] partitionKeys = context.getConfiguration().get(PRIMARY_KEY).split(",");
	    //keys.put("row_id1", ByteBufferUtil.bytes(partitionKeys[0]));
	    //keys.put("row_id2", ByteBufferUtil.bytes(partitionKeys[1]));
		//keys.put("key", ByteBufferUtil.bytes(PRIMARY_KEY));
	}
	
	public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		int sum = 0;
		for (IntWritable val : values) {
			sum += val.get();
		}
		context.write(keys, getBindVariables(key, sum));
	}
	
	private List<ByteBuffer> getBindVariables(Text word, int sum){
		List<ByteBuffer> variables = new ArrayList<ByteBuffer>();
	    keys.put(PRIMARY_KEY, ByteBufferUtil.bytes(word.toString()));
	    
	    variables.add(ByteBufferUtil.bytes(sum));         
	    return variables;
	}
}