package com.softtek.blaulabs.map;

import java.io.IOException;
import java.util.Calendar;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.datastax.driver.core.Row;

public class TimeSeriesMapperCassandra2 extends Mapper<Long, Row, Text, DoubleWritable>{
	// documentaci�n de ejemplo
	// https://examples.javacodegeeks.com/enterprise-java/apache-hadoop/hadoop-mapper-example/
    // https://gist.github.com/asig/d1ba71f2a734164e4080#file-wordcount-java-L30
	// https://gist.github.com/dkincaid/5529308
	// https://pravinchavan.wordpress.com/2013/06/18/submitting-hadoop-job-from-client-machine/
	private static final String COLUMN_SERIE_ID = "serie_id";
	private static final String COLUMN_TIMESTAMP = "timestamp";
	private static final String COLUMN_VALUE = "value";
	
	
	@Override
    public void map(Long keys, Row row, Context context) throws IOException, InterruptedException {
    	String newKey = ""; 
    	String serieId = row.getString(COLUMN_SERIE_ID);
    	Double value = row.getDouble(COLUMN_VALUE);
    	
    	Calendar cal = Calendar.getInstance();
    	cal.setTimeInMillis(row.getLong(COLUMN_TIMESTAMP));
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	
    	newKey = serieId + "_" + Long.toString(cal.getTimeInMillis());
   		context.write(new Text(newKey), new DoubleWritable(value));
    }
}
