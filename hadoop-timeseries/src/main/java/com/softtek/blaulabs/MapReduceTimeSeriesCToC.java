package com.softtek.blaulabs;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.cassandra.dht.Murmur3Partitioner;
import org.apache.cassandra.hadoop.ConfigHelper;
import org.apache.cassandra.hadoop.cql3.CqlConfigHelper;
import org.apache.cassandra.hadoop.cql3.CqlInputFormat;
import org.apache.cassandra.hadoop.cql3.CqlOutputFormat;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.softtek.blaulabs.map.TimeSeriesMapperCassandra2;
import com.softtek.blaulabs.reduce.TimeSeriesReducerCassandra;


public class MapReduceTimeSeriesCToC extends Configured implements Tool{
	
	private static final String KEYSTORE = "test_joan";
	private static final String IN_TABLE = "tkv2";
	private static final String OUT_TABLE = "tkv_min2";
	private static final String CASSANDRA_HOST = "nodemaster";
	private static final String CASSANDRA_PORT = "9160";
	
	
	
	public static void main(String[] args) throws IOException {
		//System.setProperty("hadoop.home.dir", "C:\\hadoop");		
		int res = 0;
        try {
            res = ToolRunner.run(null, new MapReduceTimeSeriesCToC(), args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(res);
    }
	
	public int run(String[] args) throws Exception {
        //conf.set("fs.maprfs.impl", "com.mapr.fs.MapRFileSystem");
		
		//con esta variable de entorno configuro el usuario que usara el job para acceder al cluster
        //System.setProperty("HADOOP_USER_NAME", "ec2-user");
		
		JobConf conf = new JobConf();
    	conf.set("fs.defaultFS", "hdfs://nodemaster:9000");
		conf.set("mapred.job.tracker", "nodemaster:9001");
		conf.set("mapreduce.framework.name", "yarn");
		conf.set("yarn.resourcemanager.address", "nodemaster:8032");
		conf.set("mapreduce.jobhistory.address", "nodemaster:10020");
		
		List<String> lSeries = new ArrayList<String>();
		lSeries.add("serie1");
		lSeries.add("serie2");
		lSeries.add("serie3");
		lSeries.add("serie4");
		lSeries.add("serie5");
		lSeries.add("serie6");
		lSeries.add("serie7");
		lSeries.add("serie8");
		lSeries.add("serie9");
		lSeries.add("serie10");
		
		for(String serie : lSeries){
			Job job = Job.getInstance(conf);
			job.setJobName("testJoan - " + serie);
			job.setJarByClass(MapReduceTimeSeriesCToC.class);

			job.setMapperClass(TimeSeriesMapperCassandra2.class);
			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(DoubleWritable.class);
		
			job.setReducerClass(TimeSeriesReducerCassandra.class);
			job.setOutputKeyClass(Map.class);
			job.setOutputValueClass(List.class);
			job.setInputFormatClass(CqlInputFormat.class);
			job.setOutputFormatClass(CqlOutputFormat.class);

			CqlConfigHelper.setInputWhereClauses(job.getConfiguration(), "serie_id='" + serie + "'");
			//CqlConfigHelper.setInputWhereClauses(job.getConfiguration(), "serie_id='serie1'");
			CqlConfigHelper.setOutputCql(job.getConfiguration(), "update " + MapReduceTimeSeriesCToC.KEYSTORE + "." + MapReduceTimeSeriesCToC.OUT_TABLE + " SET value = ? ");
		
			//ConfigHelper.setReadConsistencyLevel(job.getConfiguration(), "ONE"); 
			//ConfigHelper.setWriteConsistencyLevel(job.getConfiguration(), "ANY");
		
			ConfigHelper.setInputInitialAddress(job.getConfiguration(), MapReduceTimeSeriesCToC.CASSANDRA_HOST);
			ConfigHelper.setInputRpcPort(job.getConfiguration(), MapReduceTimeSeriesCToC.CASSANDRA_PORT);
			//ConfigHelper.setInputKeyspaceUserNameAndPassword(job.getConfiguration(), "cassandra".trim(), "cassandra".trim());
			ConfigHelper.setInputColumnFamily(job.getConfiguration(), MapReduceTimeSeriesCToC.KEYSTORE, MapReduceTimeSeriesCToC.IN_TABLE);
			ConfigHelper.setInputPartitioner(job.getConfiguration(), Murmur3Partitioner.class.getName());
		
			ConfigHelper.setOutputInitialAddress(job.getConfiguration(), MapReduceTimeSeriesCToC.CASSANDRA_HOST);
			ConfigHelper.setOutputRpcPort(job.getConfiguration(), MapReduceTimeSeriesCToC.CASSANDRA_PORT);
			//ConfigHelper.setOutputKeyspaceUserNameAndPassword(job.getConfiguration(), "cassandra".trim(), "cassandra".trim());
			ConfigHelper.setOutputColumnFamily(job.getConfiguration(), MapReduceTimeSeriesCToC.KEYSTORE, MapReduceTimeSeriesCToC.OUT_TABLE);
			ConfigHelper.setOutputPartitioner(job.getConfiguration(), Murmur3Partitioner.class.getName());
		
		
			job.submit();
		}
		//return job.waitForCompletion(true) ? 0:1;
		return 1;
	}
}
