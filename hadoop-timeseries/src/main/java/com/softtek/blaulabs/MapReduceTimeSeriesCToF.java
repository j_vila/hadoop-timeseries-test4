package com.softtek.blaulabs;


import java.io.IOException;
import java.net.URI;

import org.apache.cassandra.dht.Murmur3Partitioner;
import org.apache.cassandra.hadoop.ConfigHelper;
import org.apache.cassandra.hadoop.cql3.CqlInputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.softtek.blaulabs.map.TimeSeriesMapperCassandra2;
import com.softtek.blaulabs.reduce.TimeSeriesReducerFile;


public class MapReduceTimeSeriesCToF extends Configured implements Tool{
	
	private static final String KEYSTORE = "test_joan";
	private static final String IN_TABLE = "tkv2";
	//private static final String OUT_TABLE = "tkv_min2";
	private static final String CASSANDRA_HOST = "nodemaster";
	private static final String CASSANDRA_PORT = "9160";
	
	
	
	public static void main(String[] args) throws IOException {
		//System.setProperty("hadoop.home.dir", "C:\\hadoop");
		
		int res = 0;
        try {
            res = ToolRunner.run(null, new MapReduceTimeSeriesCToF(), args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(res);
    }
	
	public int run(String[] args) throws Exception {
		
        //conf.set("fs.maprfs.impl", "com.mapr.fs.MapRFileSystem");
		
		//con esta variable de entorno configuro el usuario que usara el job para acceder al cluster
        //System.setProperty("HADOOP_USER_NAME", "ec2-user");
		
		Configuration hdfsConf = new Configuration();
		hdfsConf.set("fs.hdfs.impl",org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
		hdfsConf.set("fs.file.impl",org.apache.hadoop.fs.LocalFileSystem.class.getName());
	    FileSystem  hdfs = FileSystem.get(URI.create("hdfs://nodemaster:9000"), hdfsConf);
	    hdfs.delete(new Path("/user/ec2-user/bb4"), true);
		
		JobConf conf = new JobConf();
    	conf.set("fs.defaultFS", "hdfs://nodemaster:9000");
		conf.set("mapred.job.tracker", "nodemaster:9001");
		conf.set("mapreduce.framework.name", "yarn");
		conf.set("yarn.resourcemanager.address", "nodemaster:8032");
		conf.set("mapreduce.jobhistory.address", "nodemaster:10020");
		FileOutputFormat.setOutputPath(conf, new Path("/user/ec2-user/bb4"));

		
        Job job = Job.getInstance(conf);
		job.setJobName("testJoan");
		job.setJarByClass(MapReduceTimeSeriesCToF.class);

		job.setMapperClass(TimeSeriesMapperCassandra2.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(DoubleWritable.class);
		
		job.setReducerClass(TimeSeriesReducerFile.class);
        job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(DoubleWritable.class);
		
		job.setInputFormatClass(CqlInputFormat.class);
		
		//CqlConfigHelper.setInputWhereClauses(job.getConfiguration(), "serie_id='serie1' AND timestamp > 1514795300000 AND timestamp <= 1514795500000");
		
		ConfigHelper.setInputInitialAddress(job.getConfiguration(), MapReduceTimeSeriesCToF.CASSANDRA_HOST);
		ConfigHelper.setInputRpcPort(job.getConfiguration(), MapReduceTimeSeriesCToF.CASSANDRA_PORT);
		//ConfigHelper.setInputKeyspaceUserNameAndPassword(job.getConfiguration(), "cassandra".trim(), "cassandra".trim());
		ConfigHelper.setInputColumnFamily(job.getConfiguration(), MapReduceTimeSeriesCToF.KEYSTORE, MapReduceTimeSeriesCToF.IN_TABLE);
		ConfigHelper.setInputPartitioner(job.getConfiguration(), Murmur3Partitioner.class.getName());
		
		//job.submit();
		return job.waitForCompletion(true) ? 0:1;
	}
}
