package com.softtek.blaulabs;


import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.cassandra.dht.Murmur3Partitioner;
import org.apache.cassandra.hadoop.ConfigHelper;
import org.apache.cassandra.hadoop.cql3.CqlConfigHelper;
import org.apache.cassandra.hadoop.cql3.CqlOutputFormat;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.softtek.blaulabs.map.TokenizerMapper;
import com.softtek.blaulabs.reduce.IntSumReducerCassandra;


public class MapReduceWordCountFToC extends Configured implements Tool{

	public static void main(String[] args) throws IOException {
		//System.setProperty("hadoop.home.dir", "C:\\hadoop");
		
		int res = 0;
        try {
        	
            res = ToolRunner.run(null, new MapReduceWordCountFToC(), args);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(res);
    }
	
	public int run(String[] args) throws Exception {

        //conf.set("fs.maprfs.impl", "com.mapr.fs.MapRFileSystem");
		//con esta variable de entorno configuro el usuario que usara el job para acceder al cluster
        //System.setProperty("HADOOP_USER_NAME", "ec2-user");
		JobConf conf = new JobConf();
    	conf.set("fs.default.name", "hdfs://nodemaster:9000");
		conf.set("mapred.job.tracker", "nodemaster:9001");
		conf.set("mapreduce.framework.name", "yarn");
		conf.set("yarn.resourcemanager.address", "nodemaster:8032");
		conf.set("mapreduce.jobhistory.address", "nodemaster:10020");
		FileInputFormat.setInputPaths(conf, new Path("/user/ec2-user/testFolder/input"));
		//FileOutputFormat.setOutputPath(conf, new Path("/user/ec2-user/bb3"));
		
        Job job = Job.getInstance(conf);
		job.setJobName("testJoan");
		job.setJarByClass(MapReduceWordCountFToC.class);
		job.setMapperClass(TokenizerMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setReducerClass(IntSumReducerCassandra.class);
		job.setOutputKeyClass(Map.class);
		job.setOutputValueClass(List.class);
		
		/*job.setInputFormatClass(CqlInputFormat.class);*/
		job.setOutputFormatClass(CqlOutputFormat.class);
		CqlConfigHelper.setOutputCql(job.getConfiguration(), "update test_joan.tkv_minute2 SET value = ?");
		
		/*ConfigHelper.setInputInitialAddress(job.getConfiguration(), "nademaster");
		ConfigHelper.setInputRpcPort(job.getConfiguration(), "9160");
		ConfigHelper.setInputKeyspaceUserNameAndPassword(job.getConfiguration(), "cassandra".trim(), "cassandra".trim());
		ConfigHelper.setInputColumnFamily(job.getConfiguration(), "test_joan", "TKV");
		ConfigHelper.setInputPartitioner(job.getConfiguration(), Murmur3Partitioner.class.getName());
		ConfigHelper.setInputSplitSize(job.getConfiguration(), 3);*/
		
		ConfigHelper.setOutputInitialAddress(job.getConfiguration(), "nodemaster");
		ConfigHelper.setOutputRpcPort(job.getConfiguration(), "9160");
		//ConfigHelper.setOutputKeyspaceUserNameAndPassword(job.getConfiguration(), "cassandra".trim(), "cassandra".trim());
		ConfigHelper.setOutputColumnFamily(job.getConfiguration(), "test_joan", "tkv_minute2");
		ConfigHelper.setOutputPartitioner(job.getConfiguration(), Murmur3Partitioner.class.getName());
		
		
		//job.submit();
		return job.waitForCompletion(true) ? 0:1;
	}
}
